class User:
    name = ""

    def __init__(self, name):
        self.name = name

    def sayHello(self):
        print("Welcome to World, " + self.name)

User1 = User("Chetan")
User1.sayHello()
