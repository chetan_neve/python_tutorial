class FtiLab():

    def method1(self):
        print("fai_v3.7")

    def method2(self, str):
        print("fai_v3.8 " + str)


def start():
    c = FtiLab()
    c.method1()
    c.method2("Released")

if __name__== "__main__":
    start()
