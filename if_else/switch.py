def SwitchExample(arg):
    switcher = {
        0: " This is Case Zero ",
        1: " This is Case One ",
        2: " This is Case Two ",
    }
    return switcher.get(arg, "nothing")


if __name__ == "__main__":

    print (SwitchExample(2))
