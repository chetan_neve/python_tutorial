singers = "Peter, Paul, and Mary"
print(singers[0:5])
#Peter
print(singers[0:3])
#Pet
print(singers[2:3])
#t
print(singers[1:2])
#e
print(singers[7:11])
#Paul
print(singers[0:15])
#Peter, Paul, an
print(singers[17:21])
#Mary

#=================
a_list = ['a', 'b', 'c', 'd', 'e', 'f']
print(a_list[1:3])
#['b', 'c']
print(a_list[:4])
#['a', 'b', 'c', 'd']
print(a_list[3:])
#['d', 'e', 'f']
print(a_list[:])
#['a', 'b', 'c', 'd', 'e', 'f']

print(a_list)
#['a', 'b', 'c', 'd', 'e', 'f']

Mickey = ("Gavar", "Pizza", 1967, "Android", 2009, "Lazy", "Beautiful, Mickey")
print(Mickey[2])
#1967
print(Mickey[1])
#Pizza
print(Mickey[0:4])
#('Gavar', 'Pizza', 1967, 'Android')
print(Mickey[5:6])
#('Lazy',)
print(Mickey[5:7])
#('Lazy', 'Beautiful, Mickey')
print(len(Mickey))
#7

Mickey = Mickey[:3] + ("Eat Pray Love", 2010) + Mickey[5:]
print(Mickey)
#('Gavar', 'Pizza', 1967, 'Eat Pray Love', 2010, 'Lazy', 'Beautiful, Mickey')

