fruit = ["apple","orange","banana","cherry"]
print([1,2]+[3,4])
#[1, 2, 3, 4]
print(fruit+[6,7,8,9])
#['apple', 'orange', 'banana', 'cherry', 6, 7, 8, 9]
print([0])
#[0]
print([0]*4)
#[0, 0, 0, 0]

alist = [1,3,5]
print(alist * 3)
#[1, 3, 5, 1, 3, 5, 1, 3, 5]

