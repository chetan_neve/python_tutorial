print("float to int")
print(3.14, int(3.14))
print("==================")
print("int to int")
print(314, int(314))
print("==================")
print("int to string")
print(314, str(314))
print("==================")
print("314", str(314))
print("==================")
print("314", int(314))
print("==================")
print("string to int")
print("314", int("314"))
print("==================")
print("string to string")
print(312, str("hello"))

