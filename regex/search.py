import re

pattern = ['software testing', 'veritas']
text = 'software testing is fun?'

for pattern in pattern:
    print('looking for "%s" in "%s" ->' % (pattern, text), end = " ")

    if re.search(pattern, text):
        print("found a match")
    else:
        print("no match")
